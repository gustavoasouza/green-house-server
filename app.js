var five = require("johnny-five")
var board = new five.Board();
var firebase = require('firebase')

board.on("ready", function () {
  console.log('Green House está pronta para frutificar!')

  var temperature = new five.Thermometer({
    pin: "A1"
  });

  temperature.on("data", function() {
    // console.log("Celcius: %d", this.C, "°C");
    // console.log("fahrenheit: %d", this.F);
    // console.log("kelvin: %d", this.K);
  });
  
  var hygrometer = new five.Hygrometer({
    controller: "TH02"
  });

  hygrometer.on("change", function() {
    console.log("Hygrometer");
    console.log("  Umidade relativa do ar : ", this.relativeHumidity);
  });

  //Declarando a variavel que recebe a porta que vai funcionar
  var luzes = new five.Led(10)

  var coolers = new five.Relay(6)

  var bombas = new five.Relay(5)

  this.repl.inject({
    luzes: luzes,
    coolers: coolers,
    bombas: bombas

  })

  //Configuração da requisição da API do firebase
  var firebaseConfig = {
    apiKey: "AIzaSyDrNQlIdd2YPix8JUAB2GEr_mWtT3ydqo8",
    authDomain: "tcc-green-house.firebaseapp.com",
    databaseURL: "https://tcc-green-house.firebaseio.com",
    projectId: "tcc-green-house",
    storageBucket: "tcc-green-house.appspot.com",
    messagingSenderId: "462184741927",
  }

  firebase.initializeApp(firebaseConfig)

  var alterarEstadoLuz = firebase.database().ref('releLuzes').on('value', function (snapshot) {
    //pegando o valor da chave lampada no firebase
    let releLuzes = snapshot.val()

    if (releLuzes == 'on') {
      //ligando a lampada caso o valor seja on
      luzes.on()
    } else {
      //desligando a lampada caso o valor seja off
      luzes.off()
    }

  })

  var alterarEstadoCoolers = firebase.database().ref('releCoolers').on('value', function (snapshot) {
    let releCoolers = snapshot.val()

    if (releCoolers == 'on') {
      coolers.on()
    } else {
      coolers.off()
    }

  })

  var alterarEstadoIrrigacao = firebase.database().ref('estadoIrrigacao').on('value', function (snapshot) {
    let estadoIrrigacao = snapshot.val()

    if (estadoIrrigacao == 'on') {
      setInterval(() => {
        bombas.on()
      }, 2000)

      bombas.off()
      
    } else {
      bombas.off()
    }

  })

})